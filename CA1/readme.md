<h1>CA1 Report</h1>

<h2>Tutorial</h2>

<strong>Week One</strong>

Welcome to this (no branches) git tutorial.


Before we start, don't forget to configure your name, email and preferred text editor using <strong>git config --global</strong>

First, create a new directory named CA1 inside the project you cloned in the last lab class and using your text editor create a readme.md file
Then, copy the project to the CA1 directory.
You can acomplish this by using some basic linux commands, like "mkdir", "cp -R", "nano readme.md".

Now let's commit these changes using the following commands:

- <strong>git add .</strong> this will add the changes you made to the project to the staging area

- <strong>git commit -m "a comment #n"</strong> this will commit the added files with a comment (it will also resolve the issue with number #n)

- <strong>git push</strong> this will upload the files to bitbucket

- <strong>git tag v1.1.0</strong> this will create the tag "v1.1.0"

- <strong>git push origin v1.1.0</strong> this will push the previously created tag to bitbucket


</br>
You're doing great. Next, add a new feature and some test to your app. 
Using the previous commands, commit and push these new changes with the tag v1.2.0.

If you ever need to delete a tag from bitbucket, you can use the command <strong>git push --delete origin tagName</strong> 

Finnaly, before you go home, add the tag "ca1-part1".

</hr>

<strong>Week Two</strong>

Welcome back! This week we are going learn about branches.

Branches are great, you can test some new stuff on your app without compromising it.
To create a branch you can use the command:

 - <strong>git branch email-field</strong>

Alternatively, you can use:

 - <strong>git checkout -b email-field</strong> which creates a branch and puts you on that branch

To push this new branch with its changes to your repository, use the command:  

 - <strong>git push origin email-field</strong>

After you've tested your changes you can merge the new branch with the main branch by switching to the main branch 
and using the command:

 - <strong>git merge email-field</strong>

This might create some conflicts that you will need to resolve before merging.
To see which file are unmerged, at any time, use the command <strong>git status</strong>.
After you've fixed the conflicts, you can push the changes to your server and use the tag v1.3.0.

If you no longer need the new branch you can delete it with the commands:

 - <strong>git branch -d email-field</strong> to delete in your machine
 - <strong>git push --delete email-field</strong> to delete it in your repository

Now create a new branch called fix-invalid-email and add some validations for your new feature.
Follow all the previous steps but this time use the tag v1.3.1.

</hr>

<h2>Alternative</h2>

<strong>Analysis</strong>

As an alternative to git, let's look at mercurial.
Mercurial and git have many similarities, both are distributed version control systems,
both are free to use, both have tags, even the commands are similar. 
To implement a similar solution to the one presented with git we would use:

 - <strong>hg add</strong> instead of git add
 - <strong>hg commit</strong> instead of git commit
 - <strong>hg push</strong> instead of git push
 - <strong>hg tag</strong> instead of git tag
 - <strong>hg branch</strong> instead of git branch
 - <strong>hg up</strong> instead of git checkout
 - <strong>hg merge</strong> instead of git merge

As you can see, if you know git, it will be very easy to learn mercurial. Not everything is similar though:
Git has more commands, which makes it more flexible but also harder to learn; Git branches are more lightweight and 
can be renamed and deleted; Git allows changes to the history of a repository beyond the simple rollback that mercurial offers.
There is also a significant difference in the amount of people that use these systems, git is much more mainstream.


</hr>

<strong>Implementation</strong>

Because bitbucket no longer supports mercurial, a repository with the same name was created in sourceforge.net.
Setting up the repository was easy but the website is too cluttered and not as easy to get around as bitbucket or github.

The TortoiseHg mercurial GUI was used instead of the command line. Although the GUI is prettier and somewhat intuitive, 
I don't feel it adds much, and I would probably use the command line if I had to work with mercurial. As for the VCS itself,
at first glance it feels very similar to git, and I would be comfortable working with it in the future.

You can find my online repository at:
https://sourceforge.net/p/devops-21-22-lmn-1211796/mercurial/ci/default/tree/

(For the sake of brevity, I did not reproduce some of the steps, such has changing the java and js code. Instead, I created some 
text files to act as placeholders.)
