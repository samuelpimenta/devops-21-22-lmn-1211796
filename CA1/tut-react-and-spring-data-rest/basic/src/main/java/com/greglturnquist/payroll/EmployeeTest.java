package com.greglturnquist.payroll;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {


    @org.junit.jupiter.api.Test
    void getFirstName() {
        Employee employee = new Employee("Samwise", "Gamgee", "Friend",
                "Gardner", 10, "email@email.com");
        String expected = "Samwise";
        String result = employee.getFirstName();
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void setFirstName() {
        Employee employee = new Employee("Samwise", "Gamgee", "Friend",
                "Gardner", 10, "email@email.com");
        employee.setFirstName("Pipin");
        String expected = "Pipin";
        String result = employee.getFirstName();
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getJobYears() {
        Employee employee = new Employee("Samwise", "Gamgee",
                "Friend", "Gardner", 10, "email@email.com");
        int expected = 10;
        int result = employee.getJobYears();
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void setJobYears() {
        Employee employee = new Employee("Samwise", "Gamgee",
                "Friend", "Gardner", 10, "email@email.com");
        employee.setJobYears(5);
        int expected = 5;
        int result = employee.getJobYears();
        assertEquals(expected, result);
        assertNotEquals(10, result);
    }

    @org.junit.jupiter.api.Test
    void getEmail() {
        Employee employee = new Employee("Samwise", "Gamgee",
                "Friend", "Gardner", 10, "email@email.com");
        String expected = "email@email.com";
        String result = employee.getEmail();
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void setEmail() {
        Employee employee = new Employee("Samwise", "Gamgee",
                "Friend", "Gardner", 10, "email@email.com");
        employee.setEmail("email2notatemail.com");
        String expected = "email@email.com";
        String result = employee.getEmail();
        assertEquals(expected, result);
        assertNotEquals("email2@email.com", result);
    }
}