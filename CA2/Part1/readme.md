# CA2 Tutorial

## Part 1

Welcome to another DevOps Tutorial. Here are the exercises and 
some steps you may take to help you.

1 - *You should start by downloading and commit to your repository (in a folder for Part
1 of CA2) the example application available at
https://bitbucket.org/luisnogueira/gradle_basic_demo/. You will be
working with this example.*

Use what was learned in the last assignment (i.e. the commands **git clone, git add, git commit,
git push**).
<br></br>

2 - *Read the instructions available in the readme.md file and experiment with the
application.*

In your terminal, use **./gradlew build, java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
and ./gradlew runClient** to test the app.
<br></br>

3 - *Add a new task to execute the server.*

You will need to access the build.gradle file and add a new task, you already have a runClient task,
which is very similar to what you need. The new one should look something like this: 

<strong>task runServer(type:JavaExec, dependsOn: classes){

   group = "DevOps"

   description = "Runs the server"

   classpath = sourceSets.main.runtimeClasspath

   mainClass = 'basic_demo.ChatServerApp'

   args '59001'

   }</strong>
   <br></br>

4 - *Add a simple unit test and update the gradle script so that it is able to execute the
test.*

After adding a unit test to the app, add a dependency to gradle using:

<strong>dependencies {

testImplementation 'junit:junit:4.12'

}</strong>

You can then use the **graddle test** command to check that everything is working as intended.
<br></br>

5 - *Add a new task of type Copy to be used to make a backup of the sources of the
application.*

To copy the contents of the src folder to a backup folder, in your build.gradle file add:

<strong>task copySrc(type: Copy) {

from 'src'

into 'backup'

}</strong>

You should then be able to perform the copy by calling this new task in the terminal.
<br></br>

6 - *Add a new task of type Zip to be used to make an archive (i.e., zip file) of the
sources of the application.*

This step is very similar to the previous one. You should be able to create a zip of
your src folder by calling the task you add to gradle, for instance:

<strong>task archiveSrc(type: Zip) {

archiveFileName  = 'src.zip'

from 'src'

destinationDirectory = file('backup')

}</strong>
<br></br>

7 - *At the end of the part 1 of this assignment mark your repository with the tag
ca2-part1.*

 - Again, use what was learned in the previous assignment (**git tag, git push origin**).