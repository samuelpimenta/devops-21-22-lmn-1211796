# CA3 Tutorial

## Part 1
<br></br>
For this week's assignment, Oracle VM VirtualBox was installed. This software is a hypervisor which allows users to 
create multiple Virtual Machines (VMs) in a single physical machine. These VMs can run a multitude of different operating 
systems (OSs) at the same time, being limited only by the hardware of the underlying "real" machine.
<br></br>

**Step 1**

For this assignment, Ubuntu was installed.
Following the lecture's instructions, an image file(ISO) was used, the amount of RAM, CPUs, network settings, etc., 
were set.
<br></br>

**Step 2**

Continuing with the online lecture's instructions: inside the newly created VM some software was installed (such as git) 
and settings were changed, allowing, among other things, the online repository to be cloned.
<br></br>

**Steps 3-6**

After some additional installations (required to run the applications) and giving execute permissions, the Spring demo 
application was run in the guest OS. It was accessed through the browser of the host OS with the IP that was set up in 
the guest OS (192.168.56.5).

Afterwards, the Gradle demo application was started in the guest OS. This OS, not having a Graphical User Interface, is
not able to launch the chat window. The host OS has one, so in order to launch the chat it was necessary to:

- open the build.gradle file
- change the runClient task to access our Virtual Machine's IP instead of localhost
- save and run the client

The chat could then be accessed in the real machine while the virtual machine was running the server.

Lastly, the gradle version of the basic spring demo application was also run in the guest OS and accessed through the 
browser of the host OS.

