#CA3 Tutorial

##Part 2
<br></br>

This tutorial will go through the steps required to use Vagrant to set up a virtual environment to execute the gradle
version of the spring boot basic tutorial application (developed in CA2, Part2).

**Vagrant** is a tool that allows for the building and maintaining of portable virtual software development environments.

It uses a **VagrantFile** to configure Virtual Machines (VMs), **Vagrant Boxes** which are packaged Vagrant
environments, **providers** which are the platforms where the virtual environment runs (VirtualBox by default), and 
**provisioners** which are tools that allow for the configuration of VMs (like ansible or puppet).
<br></br>

Before starting the actual assignment, some steps were necessary to get acquainted with vagrant:


- First, the software was installed from https://www.vagrantup.com/downloads;


- To create a vagrant environment, the command "**vagrant init** envimation/ubuntu-xenial" was executed, defining 
"envimation/ubuntu-xenial" as the box to be used in the VM; 


- The command **vagrant up** was used to start the virtualization;


- More commands were used such as, **vagrant halt** (to stop the VM), **vagrant up --provision** (after changes to the 
vagrantfile), **vagrant status** (to check the machine states), **vagrant ssh** (to jump into the VM), etc.
<br></br>

**The assignment:**

**Steps 1-3**

Vagrant was installed and the provided repository was cloned. The VagrantFile configures 2 VMs. It has some provisions
for both of them, like installing jdk 8. 

It defines a "db" VM to work as a database using H2 and a "web" VM to work as a server using Tomcat.

It also clones a repository with a gradle tutorial and builds it in the "web" VM.
<br></br>

**Step 4**

The following changes had to be made in order to run my version of the gradle demo:

- The vagrant box was changed to "ubuntu/bionic64". Because my version of the application uses jdk 11, it is not 
compatible with the older version of ubuntu in "envimation/ubuntu-xenial";


- The jdk to be installed was changed to 11;


- The repository to be cloned was changed to be mine, as well as the cd command.
<br></br>


**Step 5**

The necessary changes were made to CA2/Part2 assignment, so that the spring application uses the H2 server 
in the db VM.

With these changes, after executing **vagrant up**, 3 pages can be checked to make sure everything worked as expected:

- localhost:8080 for the apache default "It works" page;

- localhost:8080/basic-0.0.1-SNAPSHOT to check that the app is working and displaying a table;

- localhost:8080/basic-0.0.1-SNAPSHOT/h2-console to check the database.



