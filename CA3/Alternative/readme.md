#CA3 Tutorial

##Alternative - Analysis

The alternative used for this assignment was **VMware workstation player**, a free hypervisor that runs on 
Windows and Linux. Like Virtualbox, it allows us to create multiple Virtual Machines (VMs) that run in a single host.

There are many similarities between Virtualbox and VMware player: both provide hardware virtualization, both are type 2
hypervisors, both have a graphical user interface, both have shared folders between the host OS and the guest OS, etc.

There are also many differences between the two, however, most of them are due to the fact that a free VMware product
was used. Other, paid, VMware products have more features. Nevertheless, here are some differences between Virtualbox
and VMware Workstation Player:


- Virtualbox allows users to take snapshots of VMs;


- VMware has 3D graphics acceleration by default;


- Virtualbox supports several virtual disk formats, VMware only supports VMDK;


- Virtualbox allows for software virtualization, which emulates a complete computer system.


- VMware is faster, which isn't noticeable in our small academic experiments but is important for enterprises running
a large amount of VMs.


- Virtualbox is the default provider for Vagrant

For personal and academic use, virtualbox seems to be the preferred hypervisor because it offers a wider array
of free options for the user. For professional use however, VMware has a lot of paid alternatives which are preferred
by enterprises, including a type 1 hypervisor (VMware ESXi).
<br></br>

##Alternative - Implementation


These are the steps taken in order to perform this assignment:

1 - VMware Workstation Player was downloaded and installed;

2 - The VMware plugin for Vagrant was downloaded and installed;

3 - A new Vagrantfile was created with some key differences:

    - The box "hashicorp/bionic64" was used, as "ubuntu/bionic64" only works with Virtualbox

    - The IPs for the VMs were changed to match the VMware network adapter;

    - The provider was changed to "vmware_desktop".

4 - The datasource URL was changed in the project's application properties to match the IP of the db VM; 

5 - The command **vagrant up --provider vmware_desktop** was used to signal the new provider; 

6 - Once finished, the same 3 pages were checked and confirmed to be working: the index, the table and the h2 console.

