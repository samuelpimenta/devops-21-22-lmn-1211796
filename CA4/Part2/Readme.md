# CA3 Tutorial

## Part 2
<br></br>

This week, we explored **Docker Compose**, a tool developed to help define and share multi-container applications.
By using a YAML file we were able to configure 2 services to run together in an isolated environment and launch our app
from CA2 Part2 with the database running in one container and the frontend in another.
These were the steps taken to accomplish it:
<br></br>

**Step 1**

2 Dockerfiles were created, one that runs the database (db) and another for Tomcat and the application (web). Some 
changes were made to the professor's provided dockerfiles:


- For the database file, because my version of the app uses jdk-11, the ubuntu image was specified to be version "18.04" 
and jdk-11 was installed.


- For the app file, the image used was "tomcat:9.0-jdk11-temurin", so that we avoid the latest version of tomcat. Also,
the cloned repository was changed to mine and gradlew was given execute permission.
  <br></br>

**Step 2**

A docker-compose.yml file was created, one directory above the dockerfiles. This file includes the file format version
(3), defines 2 services (db and web) and provides some configurations, for instance:

- **subnet: 192.168.56.0/24** declares the network ip

- **ipv4_address: 192.168.56.10** declares the ip for the web container

- **ports:** maps container ports to host ports

- **volumes:** declares a directory on the host where container data will persist
  <br></br>

**Step 3**

The commands **docker-compose build** and **docker-compose up**  were used to build the images, create the 2 containers,
start them and configure a bridge network.
<br></br>

**Step 4**

The app was confirmed to be running on the host machine by checking http://localhost:8080/basic-0.0.1-SNAPSHOT/
<br></br>

**Step 5**

The command **docker-compose exec db bash** was used to enter the running db container and copy the database file to the
volume with the command **cp jpadb.mv.db /usr/src/data-backup**.
<br></br>

**Step 6**

The database file was confirmed to be in the data folder of the host machine and the containers were stoped and uploaded
to dockerhub. They can be seen here: 

https://hub.docker.com/repository/docker/samuelpimenta/ca4part2web

https://hub.docker.com/repository/docker/samuelpimenta/ca4part2db