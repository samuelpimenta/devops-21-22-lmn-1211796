# CA4 Alternative

## Analysis

For this assignment, kubernetes was examined.

**Kubernetes** is a portable, extensible, open source platform for managing containerized workloads and services.
It automates many of the manual processes involved in deploying, managing, and scaling containerized applications.
The containers may be, for instance, **docker** containers.

With the rise of microservices, the use of containerization also increased. Managing hundreds of containers across 
multiple environments became extremely complex and the need for technologies that would orchestrate those containers 
gave rise to tools like kubernetes.

Important concepts in kubernetes:

**Kubernetes Cluster** - a set of nodes that run containerized applications.
Kubernetes clusters are comprised of one master node and a number of worker nodes. 
The master node controls the state of the cluster; for example, which applications are running and their corresponding 
container images.
The worker nodes are the components that run these applications. Worker nodes perform tasks assigned by the master node.
There must be a minimum of one master node and one worker node for a Kubernetes cluster to be operational

**Kubelet** - Each worker node contains a kubelet, a tiny application that communicates with the master node. 
The kublet makes sure containers are running in a pod. When the control plane needs something to happen in a node, 
the kubelet executes the action.

**Pod** - a group of one or more containers, with shared storage and network resources, and a specification for how
to run the containers.

**Deployment** - a YAML (or JSON) file used to tell Kubernetes how to create or modify instances of the pods that hold a 
containerized application. Deployments can scale the number of replica pods, enable rollout of updated code in a 
controlled manner, or roll back to an earlier deployment version if necessary.

**kubectl** - the Kubernetes command-line tool. It allows users to run commands against Kubernetes clusters. It can be
used to deploy applications, inspect and manage cluster resources, and view logs.
<br></br>


**Kubernetes and Docker or Kubernetes vs Docker**´

The difference between Docker and Kubernetes is that Docker is about packaging containerized applications on a single 
node and Kubernetes is meant to run them across a cluster. They can be used independently, larger projects may benefit 
from Kubernetes and smaller ones may benefit from just adopting Docker. However, they are often used in tandem and
Docker is the most widely used container in kubernetes.

Docker does have an orchestration tool: **Docker Swarm**, which is easier to install and easier to understand
than kubernetes but has fewer functionalities and automation capabilities. Also, kubernetes is far more popular
in the industry.

##Implementation

To implement this assignment using kubernetes, we could install minikube to create a cluster locally.  

The command **minikube start --nodes=2** could be used to create a cluster with 2 nodes: 1 master and 1 worker.

We could then create a deployment. We would create a file in YAML format with specifications for a pod, for instance: 

- **metadata:** to specify things like the name of the pod

- **containers: image:** to specify the image we want for a container,

- **containerPort:** to expose a port

- **replicas:** if we want clones of a pod

We could declare a service that would allow us to access the pod by mapping a port on our machine to a pod port.

Also, we could declare a **PersistentVolume** in order to save data created in our pods.

One deployment file could be used for both the web and the db containers or we could create one for each.

We could then use the command **kubectl apply** to create the resources we declared in the deployment file.

Finally, to check they are running the command **kubectl get pods** is very useful.
