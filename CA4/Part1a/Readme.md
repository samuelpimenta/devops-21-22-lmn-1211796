# CA3 Tutorial

## Part 1
<br></br>
This week's tutorial addresses Docker. Docker is a tool that uses software virtualization to create containers.
These containers are software packages that include everything that is needed to run an application. Because they are
focused on running an application, these containers are much lighter than virtual machines, which virtualize a whole 
operating system. 

The blueprint for a container is a Docker Image. In this sense, images are similar to a Java class and the containers 
are similar to an instance of a Java class. Multiple containers can be created from an image.

To build a Docker Image, a Dockerfile is used. It is a text document with a set of instructions that Docker will use 
to create an image after using the command **docker build**.
<br></br>

**Part 1 - v1**

These are the steps taken to complete the first part of this week's assignment:

1 - Docker was installed. The recommended Windows Subsystem for Linux(WSL) 2 was selected as the backend, although 
Hyper-v was also an option. WSL2 is a full Linux kernel built by Microsoft, allowing Linux containers to run natively 
without emulation. Docker Desktop is a GUI tool that allows us to better understand what is happening with our images 
but everything was done with the CLI.

2 - A docker account was created, which allows for the upload of images to the Docker Hub Library.

3 - A Dockerfile was created with instructions for a Docker Image: 


- FROM ubuntu:18.04 - declares a starting image.


- ARG DEBIAN_FRONTEND=noninteractive - declares that there will be no interaction need during installation


- RUN apt-get update -y, RUN apt-get install git -y, RUN apt-get install openjdk-11-jdk-headless -y - installs git and jdk 11


- RUN git clone https://SamuelPimenta@bitbucket.org/samuelpimenta/devops-21-22-lmn-1211796.git - clones this repository to the container


- WORKDIR /devops-21-22-lmn-1211796/CA2/Part1/gradle_basic_demo, RUN chmod u+x gradlew, RUN ./gradlew clean build - creates an executable jar


- EXPOSE 59001 - defines the accessible port


- CMD java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001 - specifies the instructions for when the container starts


4 - The command **docker build -t my_image .** was used to build and image with the tag "my_image".

5 - The command **docker run -p 59001:59001 -d my_image** was used to start a container and declare that the port 59001 
in the host machine maps to port 59001 in the container.

6 - The app was confirmed to be running properly by using the command **/.gradlew runClient** in the host machine.

7 - Using **docker push** the image was uploaded to https://hub.docker.com/repository/docker/samuelpimenta/my_image
<br></br>

**Part 1 - v2**

For this version, the gradle demo app was copied to the folder CA4/Part1b and a Dockerfile was created there. It uses
the same ubuntu image and exposes the same port but has some differences:


- RUN apt-get update -y, RUN apt-get install openjdk-11-jdk-headless -y - because git is not needed


- COPY build/libs/basic_demo-0.1.0.jar . - because we are copying the jar from our machine rather than cloning this repository


- CMD java -cp basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001 - the path differs because only the jar exists, not the whole repository


All other commands were the same as before (except for the tag, which is "no_git"), the app was confirmed to be running
and the image was uploaded to https://hub.docker.com/repository/docker/samuelpimenta/no_git.
        