# CA5 Tutorial

## Alternative - Analysis

In this report, CircleCi is explored as an alternative to Jenkins.

CircleCI is a continuous integration and continuous delivery platform that helps development teams release code rapidly 
and automates the building, testing, and deployment. It is a cloud-based system, which means no dedicated server is required.
It offers quick configuration and maintenance without much complexity.
To start using CircleCi, an account has to be created on their website. After that, a project can be set up. The project 
should have a ".circleci" directory with a "config.yaml" file inside, which will provide the pipeline instructions.

Most of what can be done with Jenkins can be done with CircleCi and vice-versa, but there are some differences:

- Jenkins requires a dedicated server that needs constant maintenance by the allocated team. It further needs 
installation of all dependent Jenkins tools and plugins and debugging of issues.

- In Jenkins, plugins play an important role, whereas in CircleCi most functionalities are built-in.

- In Jenkins, Docker needs to be installed in the built environment. In CircleCi, docker support is built in and needs
only to be declared in the services section of the yaml file.

- The security features in Jenkins, like the encryption of credentials are superior to those of CircleCi.

- The CircleCi UI is more user-friendly, having been developed with newer technologies. Jenkins' UI is older and can be 
less responsive if a lot of plugins are installed.

- Jenkins can be used with various source-control tools. CircleCi is limited to GitHub and Bitbucket
<br><br/>

## Alternative - Implementation

For lack of time, not all steps were reproduced for this alternative, only the assemble, jar, test and 
test-results publishing steps.

Here is what was done in more detail:

- Because CircleCi was not finding the yml file in this directory (CA5/Alternative), a new repository was created for 
the purpose of this exercise. It can be seen here: https://bitbucket.org/samuelpimenta/ca5-part2-alternative

- An account was created and configured on CircleCi.

- A ".circleci" directory was created at the root.

- Inside, a config.yml file was created: 


    version: 2.1
    jobs:
    build:
    
        working_directory: ~/circleci-devops
    
        docker:
          - image: circleci/openjdk:11-stretch
    
        steps:
    
          - checkout
    
          - run: chmod u+x gradlew
    
          - run: ./gradlew clean assemble
    
          - run: ./gradlew jar
    
          - run: ./gradlew test
    
          - store_test_results:
              path: build/test-results/test

- This file provides instructions for CircleCi to execute. For instance "- image: circleci/openjdk:11-stretch" specifies
that a container with jdk-11 should be used. Similarly to the Jenkinsfile, we also have steps like 
"- run: ./gradlew clean assemble"

- Unlike jenkins, CircleCi runs on every push without need for configuration. After a few attempts, a successful build 
was executed:

![img.png](img.png)

It might be because of the nicer UI, the simpler setup or both, but even though I did not spend as much time as I would 
have liked on this alternative, I enjoyed using CircleCi and actually preferred it to Jenkins.