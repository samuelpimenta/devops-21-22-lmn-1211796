# CA5 Tutorial

## Part 2

This week's assignment combined two previous assignments: CA5 Part1 and CA4 Part1. Both a Jenkinsfile and a Dockerfile
were prepared, to run a pipeline that, among other steps, ran a docker container. Because of previous assignments, I was 
more familiar with the gradle-basic-demo so that was the app I used instead of the app from CA2 Part2. This time, rather
than placing the Jenkinsfile in CA2 Part1, the folder was copied into the CA5 Part2 directory.
These are the steps taken to complete the assignment.
<br></br>

**Step 1**

Some configurations were managed in jenkins in order to complete the assignment:

- The HTML Publisher plugin was installed.
- The Docker Pipeline plugin was also installed.
- Docker's credentials were stored in Jenkins with the id "docker_creds"
<br></br>


**Step 2**

A Dockerfile was created, which was similar to the one create for CA4 Part1-b:

    # Ubuntu 18.04 because of jdk 11
    FROM ubuntu:18.04
    # Only the jdk is need for this
    RUN apt-get update -y
    RUN apt-get install openjdk-11-jdk-headless -y
    
    # Copies the executable jar from the host machine to the container
    COPY build/libs/basic_demo-0.1.0.jar .
    
    # Declares the accessible port
    EXPOSE 59001
    
    CMD java -cp basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001

There was no need to clone the repository because it will already be cloned in the first step of the pipeline.
It is only necessary to choose an image (Ubuntu 18.04), install jdk11, copy the generated jar to the container,
expose a port and run the app.
<br></br>


**Step 3**

A Jenkinsfile was also created with many similarities to the previous one. Except for the path in dir, the following
stages were the same: checkout, assemble, test and archive. The post at the end was also similar.
<br></br>


**Step 4**

Some stages were added to the Jenkinsfile, which is the core of this assignment:

    stage('Jar') {
        steps {
            dir('CA5/Part2/gradle_basic_demo') {
                bat './gradlew jar'
            }
        }
    }

This step is necessary because there is no build stage. A jar file has to be generated so docker can run the app later.
<br></br>

    stage('Javadoc') {
        steps{
            echo 'Generating Javadoc...'
            dir('CA5/Part2/gradle_basic_demo') {
                bat './gradlew javadoc'
            }
            publishHTML (target : [allowMissing: false,
            alwaysLinkToLastBuild: true,
            keepAll: true,
            reportDir: 'CA5/Part2/gradle_basic_demo/build/docs/javadoc',
            reportFiles: 'index.html',
            reportName: 'myReports',
            reportTitles: 'The Report'])
        }
    }

This step generates the javadoc using "./gradlew javadoc". It then publishes the docs in jenkins with "publishHTML".
<br></br>

    stage ('Docker') {
        steps {
            dir('CA5/Part2/gradle_basic_demo') {
                script {
                docker.withRegistry('https://registry.hub.docker.com', 'docker_creds') {
                    def theImage = docker.build("samuelpimenta/ca5part2:${env.BUILD_ID}")
                    theImage.push()
                    }
                }
            }
        }
    }

This is the most complex step and involves running the Dockerfile script with "docker.build", storing the image in a 
variable (theImage) and then pushing it to Docker Hub using the credentials stored in Step 1. Note the use of the 
jenkins environment variable "BUILD_ID" to tag the image.
<br></br>


**Step 5**

Finally, everything was pushed to the repository, run in jenkins and confirmed to be working. The publish docker image
can be seen here: https://hub.docker.com/repository/docker/samuelpimenta/ca5part2