# CA5 Tutorial

## Part 1

For this assignment we used **Jenkins**. Jenkins is  an open-source automation server. It is the most famous 
Continuous Integration tool. It helps automate the parts of software development related to building, testing, and 
deploying. Although initially written for Java, it can be used with most languages and, because of the large amount
of available plugins, Jenkins can be used with all sorts of tools: git, maven, gradle, bitbucket, etc.

These were the steps taken to complete this jenkins assignment:

**Step1**

Jenkins was set up, following the lecture's instructions and the instructions on 
https://www.jenkins.io/doc/book/installing/
<br><br/>


**Step2**

A couple of example jobs were created to execute pipelines with scripts created directly in the job configuration page.
The scripts were the ones provided in the online lecture.
<br><br/>


**Step3**

A **Jenkinsfile** was created in the CA2 Part1 project. It includes instructions for jenkins to execute, such as:

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git branch: 'main', url: 'https://bitbucket.org/samuelpimenta/devops-21-22-lmn-1211796'
            }
        }
It was not necessary to define credentials as the repository is public, due to a previous assignment.
It was necessary, however, to declare the name of the branch (main), because the repository does not have a branch named 
master.
<br><br/>

        stage('Assemble') {
            steps {
                echo 'Assembling...'
                dir('CA2/Part1/gradle_basic_demo') {
                    bat './gradlew clean assemble'
                }

            }
        }
This step builds the project but does not run the tests. Note that in order to run the command "./gradlew", it was 
necessary to indicate the directory where it is located, using the **dir** instruction.
Also note that "bat" is being used instead of "sh" as my operating system is windows. 
<br><br/>

        stage('Test') {
            steps {
                echo 'Testing...'
                dir('CA2/Part1/gradle_basic_demo') {
                    bat './gradlew test'
                }
            }
        }
This step is similar to the previous one ("dir" and "bat" are also used), but this time it's running the tests.
<br><br/>

        stage('Archiving') {
            steps {
                echo 'Archiving...'
                dir('CA2/Part1/gradle_basic_demo') {
                    archiveArtifacts 'build/distributions/*'
                }
            }
        }
    }
This allows the specified artifacts to be accessible from the Jenkins webpage, i.e. a .zip and .tar file
<br><br/>

    post {
        always {
            junit 'CA2/Part1/gradle_basic_demo/build/test-results/test/*.xml'
        }
    }
Finally, this instruction allows jenkins to publish the app's test results.
<br><br/>


**Step 4**

The Jenkinsfile was commited to bitbucket.
<br><br/>


**Step 5**

A new pipeline item was created in jenkins, but this time selecting "Pipeline Script from SCM". Some options were then
setup: Git was chosen as the SCM, my repository URL was indicated and the path to the Jenkinsfile as well.
<br><br/>


**Step 6**

A build was scheduled in Jenkins and everything was compiled and posted successfully.

